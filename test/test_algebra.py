import pytest
import sys
from context import tushu
from tushu.gushi import dice_algebra
parser = dice_algebra.parser
lexer = dice_algebra.lexer

roll_strings=[
        ('2',2,2),
        ('2+2',4,4),
        ('2-2',0,0),
        ('2/2',1,1),
        ('2^2',4,4),
        ('1d1',1,1),
        ('1d6',1,6),
        ('3d6d20',3,360),
        ('1d1+1d1',2,2),
        ('3d6k1',1,6),
        ('10d6s3',3,60),
        ('10d6f3',0,60),
        ('10d6k5s3',0,30),
        ('10d6k5f3',0,30),
        ('3d6l1',1,6),
        ('3d6r1',3,18),
        ('3d6!1',3,sys.maxsize),
        ('1d1!1',1,1),
        ('1d20',1,20),
        ('3d20',3,60),
        ('1<4?1d1:2d1',1,1),
        ('~1<4?1d1:2d1',2,2),
        ]
@pytest.mark.parametrize("dice_string, expected_min, expected_max",roll_strings)
def test_algebra(dice_string, expected_min, expected_max):
    roll = parser.parse(lexer.lex(dice_string))
    roll_eval = roll.eval()
    assert roll_eval>=expected_min
    assert roll_eval<=expected_max

logic_strings=[
        ('1d1+1d1>4',False),
        ('1>4',False),
        ('1<4',True),
        ('~1>4',True),
        ('~1<4',False),
        ('1=4',False),
        ('4=4',True),
        ('1=1&4=4',True),
        ('1=1|4=4',True),
        ('1=4&4=4',False),
        ('1=4|4=4',True),
        ('~1=1&4=4',False),
        ('~1=1|4=4',False),
        ('~1=4&4=4',True),
        ('~1=4|4=4',False),
        ('~1=4',True),
        ('~4=4',False)
        ]
@pytest.mark.parametrize("dice_string, expected_result",logic_strings)
def test_logic(dice_string, expected_result):
    roll = parser.parse(lexer.lex(dice_string))
    roll_eval = roll.eval()
    assert roll_eval == expected_result
