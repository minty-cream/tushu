import motor.motor_asyncio
import mongomock
import asyncio
import pytest
from unittest import mock
from context import tushu
from tushu.handle_message import handle_message
from datasets import config_sets

def get_send_message(expected_recipient, expected_message, configs, expected_configs, characters, expected_characters):
    async def send_message(recipient, message):
        assert recipient==expected_recipient
        assert expected_message in message
        for expected_config in expected_configs:
            print("expected_config", expected_config)
            found_config = await configs.find_one(expected_config)
            print("found_config", found_config)
            assert found_config is not None
            del found_config['_id']
            assert found_config == expected_config
        for expected_character in expected_characters:
            print("expected_character", expected_character)
            found_character = await characters.find_one(expected_character)
            print("found_character", found_character)
            assert found_character is not None
            del found_character['_id']
            assert found_character == expected_character
    return send_message

message_defaults = {
        'author':{
            'id':'111',
            'name':'Test Author'
            },
        'channel':{
            'id':'222',
            'name':'Test Channel'
            },
        'server':{
            'id':'config',
            'name':'Test Server'
            }
        }

@pytest.mark.asyncio
@pytest.mark.parametrize("message_string, recipient, expected_message, default_configs, expected_configs, default_characters, expected_characters",config_sets.data)
async def test_rolls(message_string, recipient, expected_message, default_configs, expected_configs, default_characters, expected_characters):
    message = mock.MagicMock()
    message.content = message_string
    message.author = mock.MagicMock(**message_defaults['author'])
    message.channel = mock.MagicMock(**message_defaults['channel'])
    message.server = mock.MagicMock(**message_defaults['server'])
    message.server.owner = message.author
    expected_recipient = getattr(message,recipient,None)

    mongo_client = motor.motor_asyncio.AsyncIOMotorClient('mongodb://localhost:27017')
    db = mongo_client['tushu_automated_tests']
    await db.drop_collection('configs.config')
    await db.drop_collection('characters.config')
    try:
        await db['configs'][message.server.id].insert_many(default_configs)
    except TypeError as e:
        print ("Warning: ",e)
        pass

    try:
        await db['characters'][message.server.id].insert_many(default_characters)
    except TypeError as e:
        print ("Warning: ",e)
        pass


    send_message = get_send_message(
            expected_recipient,
            expected_message,
            db['configs'][message.server.id],
            expected_configs,
            db['characters'][message.server.id],
            expected_characters,
            )

    client = mock.MagicMock()
    client.send_message = send_message
    await handle_message(client,message,db)
