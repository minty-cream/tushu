import motor.motor_asyncio
import mongomock
import asyncio
import pytest
from unittest import mock
from context import tushu
from tushu.handle_message import handle_message
from datasets import mention_sets

def get_send_message(expected_recipient, expected_message):
    async def send_message(recipient, message):
        assert recipient==expected_recipient
        assert expected_message in message

    return send_message

message_defaults = {
        'author':{
            'id':'111',
            'name':'Test Author',
            },
        'channel':{
            'id':'222',
            'name':'Test Channel'
            },
        'server':{
            'id':'status',
            'name':'Test Server'
            }
        }

@pytest.mark.asyncio
@pytest.mark.parametrize("message_string, recipient, expected_message, default_configs, default_characters",mention_sets.data)
async def test_rolls(message_string, recipient, expected_message, default_configs, default_characters):
    message = mock.MagicMock()
    message.content = message_string
    message.mentions=[mock.MagicMock(id='mention_id',name='Mentioned Author')]
    message.author = mock.MagicMock(**message_defaults['author'])
    message.channel = mock.MagicMock(**message_defaults['channel'])
    message.server = mock.MagicMock(**message_defaults['server'])
    expected_recipient = getattr(message,recipient,None)

    mongo_client = motor.motor_asyncio.AsyncIOMotorClient('mongodb://localhost:27017')
    db = mongo_client['tushu_automated_tests']
    await db.drop_collection('configs.status')
    await db.drop_collection('characters.status')
    try:
        await db['configs'][message.server.id].insert_many(default_configs)
        await db['characters'][message.server.id].insert_many(default_characters)
    except TypeError:
        pass


    send_message = get_send_message(
            expected_recipient,
            expected_message
            )

    client = mock.MagicMock()
    client.send_message = send_message
    await handle_message(client,message,db)
