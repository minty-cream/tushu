import motor.motor_asyncio
import mongomock
import asyncio
import pytest
from unittest import mock
from context import tushu
from tushu.handle_message import handle_message
from datasets import doc_sets

def get_send_message(expected_recipient, expected_message):
    async def send_message(recipient, message):
        assert recipient==expected_recipient
        assert expected_message in message

    return send_message

message_defaults = {
        'author':{
            'id':'111',
            'name':'Test Author'
            },
        'channel':{
            'id':'222',
            'name':'Test Channel'
            },
        'server':{
            'id':'docs',
            'name':'Test Server'
            }
        }

@pytest.mark.asyncio
@pytest.mark.parametrize("message_string, recipient, expected_message",doc_sets.data)
async def test_docs(message_string, recipient, expected_message):
    message = mock.MagicMock()
    message.content = message_string
    message.author = mock.MagicMock(**message_defaults['author'])
    message.channel = mock.MagicMock(**message_defaults['channel'])
    message.server = mock.MagicMock(**message_defaults['server'])
    expected_recipient = getattr(message,recipient,None)
    send_message = get_send_message(
            expected_recipient,
            expected_message
            )

    client = mock.MagicMock()
    client.send_message = send_message

    mongo_client = motor.motor_asyncio.AsyncIOMotorClient('mongodb://localhost:27017')
    db = mongo_client['tushu_automated_tests']
    await db.drop_collection('configs.docs')
    await db.drop_collection('characters.docs')
    await handle_message(client,message,db)
