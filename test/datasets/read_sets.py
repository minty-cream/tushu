data=[
        #test_algebra can handle all these complexities, this should focus only on bot commands
        (
            '/roll 1d1',
            'channel',
            '(1d1->[1]->1)\nResult: 1',
            None,
            None
            ),
        (
            '/roll (1+1)d1',
            'channel',
            '((1+1)d1->[1,1]->2)\nResult: 2',
            None,
            None
            ),
        (
            '/roll {athletics}d1',
            'channel',
            '(3d1->[1,1,1]->3)\nResult: 3',
            [
                {
                    'field':'athletics',
                    'type':'number'
                    },
                ],
            [
                {
                    'name':'Test Character',
                    'author_id':'111',
                    'active':True,
                    'athletics':3
                    }
                ]
            ),
        (
            '/roll {athletics}d1',
            'channel',
            '3+(3d1->[1,1,1]->3)\nResult: 6',
            [
                {
                    'field':'strength',
                    'type':'number'
                    },
                {
                    'field':'dexterity',
                    'type':'number'
                    },
                ],
            [
                {
                    'name':'Test Character',
                    'author_id':'111',
                    'active':True,
                    'strength':3,
                    'dexterity':3,
                    'athletics':'{strength}+{dexterity}'
                    }
                ]
            ),
        (
                '/roll {athletics}d1',
                'channel',
                '((3+3)d1->[1,1,1,1,1,1]->6)\nResult: 6',
                [
                    {
                        'field':'strength',
                        'type':'number'
                        },
                    {
                        'field':'dexterity',
                        'type':'number'
                        },
                    ],
                [
                    {
                        'name':'Test Character',
                        'author_id':'111',
                        'active':True,
                        'strength':3,
                        'dexterity':3,
                        'athletics':'({strength}+{dexterity})'
                        }
                    ]
                ),
        (
                '/roll Test 1d1',
                'channel',
                '(1d1->[1]->1)\nResult: 1',
                [
                    {
                        'field':'athletics',
                        'type':'number'
                        },
                    ],
                [
                    {
                        'name':'Test',
                        'author_id':'111',
                        'athletics':3
                        }
                    ]
                ),
        (
                '/roll Test {athletics}d1',
                'channel',
                '(3d1->[1,1,1]->3)\nResult: 3',
                [
                    {
                        'field':'athletics',
                        'type':'number'
                        },
                    ],
                [
                    {
                        'name':'Test',
                        'author_id':'111',
                        'athletics':3
                        }
                    ]
                ),
        (
                '/roll Test {athletics}d1',
                'channel',
                '3+(3d1->[1,1,1]->3)\nResult: 6',
                [
                    {
                        'field':'strength',
                        'type':'number'
                        },
                    {
                        'field':'dexterity',
                        'type':'number'
                        },
                    ],
                [
                    {
                        'name':'Test',
                        'author_id':'111',
                        'strength':3,
                        'dexterity':3,
                        'athletics':'{strength}+{dexterity}'
                        }
                    ]
                ),
        (
                '/roll Test {athletics}d1',
                'channel',
                '((3+3)d1->[1,1,1,1,1,1]->6)\nResult: 6',
                [
                    {
                        'field':'strength',
                        'type':'number'
                        },
                    {
                        'field':'dexterity',
                        'type':'number'
                        },
                    ],
                [
                    {
                        'name':'Test',
                        'author_id':'111',
                        'strength':3,
                        'dexterity':3,
                        'athletics':'({strength}+{dexterity})'
                        }
                    ]
                ),
        (
                '/roll Test 1d1',
                'channel',
                '(1d1->[1]->1)\nResult: 1',
                [
                    {
                        'field':'athletics',
                        'type':'number'
                        },
                    ],
                [
                    {
                        'name':'Test',
                        'author_id':'missing',
                        'athletics':3
                        }
                    ]
                ),
        (
                '/roll Test {athletics}d1',
                'channel',
                '(0d1->[]->0)\nResult: 0',
                [
                    {
                        'field':'athletics',
                        'type':'number'
                        },
                    ],
                [
                    {
                        'name':'Test',
                        'author_id':'missing',
                        'athletics':3
                        }
                    ]
                ),
        (
                '/roll Test {athletics}g1',
                'channel',
                'Incorrect dice algebra.',
                [
                    {
                        'field':'strength',
                        'type':'number'
                        },
                    {
                        'field':'dexterity',
                        'type':'number'
                        },
                    ],
                [
                    {
                        'name':'Test',
                        'author_id':'missing',
                        'strength':3,
                        'dexterity':3,
                        'athletics':'({strength}+{dexterity})'
                        }
                    ]
                ),
        (
            '/config',
            'channel',
            'Test character has not been created.',
            [ ],
            [ ]
            ),
        (
            '/config',
            'channel',
            {
                'content': None,
                'embed': {'description': '**Player** <@business>\n:x: **Not Approved**\n',
                    'fields': [{'inline': True, 'name': 'Athletics', 'value': '3'}],
                    'title': '**Character** TEST_CHARACTER',
                    'type': 'rich'}
                },
            [
                {
                    'field':'athletics',
                    'type':'number'
                    },
                ],
            [
                {
                    'name':'TEST_CHARACTER',
                    'author_id':'business',
                    'active':False,
                    'approved':False,
                    'athletics':3
                    }
                ]
            ),
        (
            '/sheet',
            'channel',
            {
                'content': None,
                'embed': {'description': '**Player** <@111>\n:x: **Not Approved**\n',
                     'fields': [{'inline': True, 'name': 'Athletics', 'value': '3'}],
                      'title': '**Character** Personal Test',
                       'type': 'rich'}
                },
            [
                {
                    'field':'athletics',
                    'type':'number'
                    },
                ],
            [
                {
                    'name':'Personal Test',
                    'author_id':'111',
                    'active':True,
                    'approved':False,
                    'athletics':3
                    }
                ]
            ),
        ]
