data=[
        (
            '/help',
            'author',
            'Tu Shu is a roll bot that stores your character sheets and has a powerful macro language.\n'
            ),
        (
            '/help dice_algebra',
            'author',
            'Dice algebra is a powerful macro language that allows you to get random numbers, do math, deal with variables, and make decisions based on boolean algebra\n'
            ),
        (
            '/help configure',
            'author',
            'Configure the list of fields on the character sheet\n'
            ),
        ]
