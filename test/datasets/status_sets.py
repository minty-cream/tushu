data=[
        (
            '/ooc',
            'channel',
            'is now OOC',
            [
                {
                    'field':'athletics',
                    'type':'number'
                    },
                ],
            [
                {
                    'name':'Test Character',
                    'author_id':'111',
                    'active':True,
                    'valid':True,
                    'athletics':3
                    }
                ],
            {
                'name':'Test Character',
                'author_id':'111',
                'active':False,
                'valid':True,
                'athletics':3
                }
            ),
        (
            '/alt Character',
            'channel',
            'is now playing as Character',
            [
                {
                    'field':'athletics',
                    'type':'number'
                    },
                ],
            [
                {
                    'name':'Character',
                    'author_id':'111',
                    'active':False,
                    'valid':True,
                    'athletics':3
                    }
                ],
            {
                'name':'Character',
                'author_id':'111',
                'active':True,
                'valid':True,
                'athletics':3
                }
            ),
        (
            '/alt Fake',
            'channel',
            'Character was not found.',
            [
                {
                    'field':'athletics',
                    'type':'number'
                    },
                ],
            [
                {
                    'name':'Character',
                    'author_id':'111',
                    'active':False,
                    'valid':True,
                    'athletics':3
                    }
                ],
            {
                'name':'Character',
                'author_id':'111',
                'active':False,
                'valid':True,
                'athletics':3
                }
            ),
        (
            '/alt "Test Character"',
            'channel',
            'is now playing as Test Character',
            [
                {
                    'field':'athletics',
                    'type':'number'
                    },
                ],
            [
                {
                    'name':'Test Character',
                    'author_id':'111',
                    'active':False,
                    'valid':True,
                    'athletics':3
                    }
                ],
            {
                'name':'Test Character',
                'author_id':'111',
                'active':True,
                'valid':True,
                'athletics':3
                }
            ),
        ]
