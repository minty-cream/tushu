data=[
        (
            '/alt @Test Author',
            'channel',
            'is playing as Character',
            [
                {
                    'field':'athletics',
                    'type':'number'
                    },
                ],
            [
                {
                    'name':'Character',
                    'author_id':'mention_id',
                    'active':True,
                    'valid':True,
                    'athletics':3
                    }
                ],
            ),
        (
            '/alt @Test Author',
            'channel',
            'is not playing as anyone.',
            [
                {
                    'field':'athletics',
                    'type':'number'
                    },
                ],
            [
                {
                    'name':'Character',
                    'author_id':'111',
                    'active':True,
                    'valid':True,
                    'athletics':3
                    }
                ],
            ),
        ]
