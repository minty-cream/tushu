data=[
        (
            '/configure Strength:=2 Dexterity:=2 Athletics:={Strength}+{Dexterity} Description:=Fill Me Math:=2+2',
            'channel',
            'Configuration complete. View test character with `/config`. All characters have been unapproved.',
            [],
            [
                {
                    'field':'strength',
                    'type':'number',
                    'default':'2'
                    },
                {
                    'field':'dexterity',
                    'type':'number',
                    'default':'2'
                    },
                {
                    'field':'athletics',
                    'type':'calculation',
                    'default':'{strength}+{dexterity}'
                    },
                {
                    'field':'description',
                    'type':'text',
                    'default':'Fill Me'
                    },
                {
                    'field':'math',
                    'type':'number',
                    'default':'4'
                    },
                ],
            [
                {
                    'name':'Character',
                    'author_id':'111',
                    'active':True,
                    'approved':True
                    }
                ],
            [
                {
                    'name':'TEST_CHARACTER',
                    'author_id':'config',
                    'strength':'2',
                    'dexterity':'2',
                    'athletics':'4',
                    'description':'Fill Me',
                    'math':'4',
                    'approved': False,
                    'waiting_for_approval': False
                    },
                {
                    'name':'Character',
                    'author_id':'111',
                    'active':False,
                    'approved':False
                    }
                ],
            ),
        (
                '/configure Strength:=',
                'channel',
                'Configuration complete. View test character with `/config`. All characters have been unapproved.',
                [
                    {
                        'field':'strength',
                        'type':'number',
                        'default':'2'
                        },
                    {
                        'field':'dexterity',
                        'type':'number',
                        'default':'2'
                        },
                    {
                        'field':'athletics',
                        'type':'calculation',
                        'default':'{strength}+{dexterity}'
                        },
                    {
                        'field':'description',
                        'type':'text',
                        'default':'Fill Me'
                        },
                    {
                        'field':'math',
                        'type':'number',
                        'default':'4'
                        },
                    ],
                [
                    {
                        'field':'dexterity',
                        'type':'number',
                        'default':'2'
                        },
                    {
                        'field':'athletics',
                        'type':'calculation',
                        'default':'{strength}+{dexterity}'
                        },
                    {
                        'field':'description',
                        'type':'text',
                        'default':'Fill Me'
                        },
                    {
                        'field':'math',
                        'type':'number',
                        'default':'4'
                        },
                    ],
                [
                        {
                            'name':'TEST_CHARACTER',
                            'author_id':'config',
                            'strength': '2',
                            'dexterity':'2',
                            'athletics':'4',
                            'description':'Fill Me',
                            'math':'4'
                            },
                        {
                            'name':'Character',
                            'author_id':'111',
                            'active':True,
                            'approved':True
                            }
                        ],
                [
                        {
                            'name':'TEST_CHARACTER',
                            'author_id':'config',
                            'dexterity':'2',
                            'athletics':'2',
                            'description':'Fill Me',
                            'math':'4',
                            'approved': False,
                            'waiting_for_approval': False
                            },
                        {
                            'name':'Character',
                            'author_id':'111',
                            'active':False,
                            'approved':False
                            }
                        ],
                ),
        (
                '/edit Strength:=',
                'author',
                'Character was succesfully updated. View their sheet with `/sheet`.',
                [
                    {
                        'field':'strength',
                        'type':'number',
                        'default':'2'
                        },
                    {
                        'field':'dexterity',
                        'type':'number',
                        'default':'2'
                        },
                    {
                        'field':'athletics',
                        'type':'calculation',
                        'default':'{strength}+{dexterity}'
                        },
                    {
                        'field':'description',
                        'type':'text',
                        'default':'Fill Me'
                        },
                    {
                        'field':'math',
                        'type':'number',
                        'default':'4'
                        },
                    ],
                [
                    {
                        'field':'strength',
                        'type':'number',
                        'default':'2'
                        },
                    {
                        'field':'dexterity',
                        'type':'number',
                        'default':'2'
                        },
                    {
                        'field':'athletics',
                        'type':'calculation',
                        'default':'{strength}+{dexterity}'
                        },
                    {
                        'field':'description',
                        'type':'text',
                        'default':'Fill Me'
                        },
                    {
                        'field':'math',
                        'type':'number',
                        'default':'4'
                        },
                    ],
                [
                        {
                            'name':'Character',
                            'author_id':'111',
                            'approved':False,
                            'active':True,
                            'strength': '2',
                            'dexterity':'2',
                            'athletics':'4',
                            'description':'Fill Me',
                            'math':'4'
                            },
                        ],
                [
                        {
                            'name':'Character',
                            'author_id':'111',
                            'waiting_for_approval': False,
                            'approved':False,
                            'active':True,
                            'dexterity':'2',
                            'athletics':'2',
                            'description':'Fill Me',
                            'math':'4'
                            },
                        ],
                ),
        (
                '/edit Strength:=2 Dexterity:=2 Description:=Fill Me Math:=2+2',
                'author',
                'Character was succesfully updated. View their sheet with `/sheet`.',
                [
                    {
                        'field':'strength',
                        'type':'number',
                        'default':'2'
                        },
                    {
                        'field':'dexterity',
                        'type':'number',
                        'default':'2'
                        },
                    {
                        'field':'athletics',
                        'type':'calculation',
                        'default':'{strength}+{dexterity}'
                        },
                    {
                        'field':'description',
                        'type':'text',
                        'default':'Fill Me'
                        },
                    {
                        'field':'math',
                        'type':'number',
                        'default':'4'
                        },
                    ],
                [
                    {
                        'field':'strength',
                        'type':'number',
                        'default':'2'
                        },
                    {
                        'field':'dexterity',
                        'type':'number',
                        'default':'2'
                        },
                    {
                        'field':'athletics',
                        'type':'calculation',
                        'default':'{strength}+{dexterity}'
                        },
                    {
                        'field':'description',
                        'type':'text',
                        'default':'Fill Me'
                        },
                    {
                        'field':'math',
                        'type':'number',
                        'default':'4'
                        },
                    ],
                [
                        {
                            'name':'Character',
                            'author_id':'111',
                            'active':True,
                            'approved':True
                            }
                        ],
                [
                        {
                            'name':'Character',
                            'author_id':'111',
                            'active':True,
                            'approved':False,
                            'waiting_for_approval': False,
                            'strength':'2',
                            'dexterity':'2',
                            'athletics':'4',
                            'description':'Fill Me',
                            'math':'4'
                            },
                        ],
                ),
        (
                '/generate "Two Name" Athletics:={Strength}+{Dexterity} Strength:=2 Dexterity:=2 Description:=Fill Me Math:=2+2',
                'channel',
                'Two Name created. Select them with `/alt Two Name`',
                [
                    {
                        'field':'strength',
                        'type':'number',
                        'default':'2'
                        },
                    {
                        'field':'dexterity',
                        'type':'number',
                        'default':'2'
                        },
                    {
                        'field':'description',
                        'type':'text',
                        'default':'Fill Me'
                        },
                    {
                        'field':'math',
                        'type':'number',
                        'default':'4'
                        },
                    ],
                [
                    {
                        'field':'strength',
                        'type':'number',
                        'default':'2'
                        },
                    {
                        'field':'dexterity',
                        'type':'number',
                        'default':'2'
                        },
                    {
                        'field':'description',
                        'type':'text',
                        'default':'Fill Me'
                        },
                    {
                        'field':'math',
                        'type':'number',
                        'default':'4'
                        },
                    ],
                [],
                [
                    {'author_id': '111',
                        'athletics': '{strength}+{dexterity}',
                        'dexterity': '2',
                        'active': False,
                        'math': '4',
                        'strength': '2',
                        'description': 'Fill Me',
                        'waiting_for_approval': False,
                        'name': 'Two Name',
                        'approved': False
                        }
                    ],
                ),
        (
                '/generate OneName Athletics:={Strength}+{Dexterity} Strength:=2 Dexterity:=2 Description:=Fill Me Math:=2+2',
                'channel',
                'OneName created. Select them with `/alt OneName`',
                [
                    {
                        'field':'strength',
                        'type':'number',
                        'default':'2'
                        },
                    {
                        'field':'dexterity',
                        'type':'number',
                        'default':'2'
                        },
                    {
                        'field':'description',
                        'type':'text',
                        'default':'Fill Me'
                        },
                    {
                        'field':'math',
                        'type':'number',
                        'default':'4'
                        },
                    ],
                [
                    {
                        'field':'strength',
                        'type':'number',
                        'default':'2'
                        },
                    {
                        'field':'dexterity',
                        'type':'number',
                        'default':'2'
                        },
                    {
                        'field':'description',
                        'type':'text',
                        'default':'Fill Me'
                        },
                    {
                        'field':'math',
                        'type':'number',
                        'default':'4'
                        },
                    ],
                [],
                [
                    {'author_id': '111',
                        'athletics': '{strength}+{dexterity}',
                        'dexterity': '2',
                        'active': False,
                        'math': '4',
                        'strength': '2',
                        'description': 'Fill Me',
                        'waiting_for_approval': False,
                        'name': 'OneName',
                        'approved': False
                        }
                    ],
                ),
        ]
