import daemon
import os
import signal
import sys

from dotenv import load_dotenv
from pid import PidFile
from lockfile.pidlockfile import PIDLockFile
from main import do_main
from datetime import datetime

def run_daemon(arg):
    load_dotenv()
    lockpath = os.getenv('LOCKPATH')
    logpath = open(os.getenv('LOGPATH'),"a")
    errpath = open(os.getenv('ERRPATH'),"a")
    timestamp = str(datetime.now())+"\n"

    pidfile=PIDLockFile(lockpath)
    if arg == 'status':
        if pidfile.is_locked():
            print('Tushu is currently running. Stop it with the `stop` command.')
        else:
            print('Tushu is not running. Start with the `start` command.')
    else:
        ctx = daemon.DaemonContext(
                pidfile=pidfile,
                stdout=logpath,
                stderr=errpath
                )

        if arg == 'stop':
            if pidfile.is_locked():
                pid = pidfile.read_pid()
                os.kill(pid, signal.SIGTERM)
                print('Tushu has been stopped.')
            else:
                print('Tushu is not running. Start with the `start` command.')

        elif arg == 'start':
            if pidfile.is_locked():
                print('Tushu is currently running. Stop it with the `stop` command.')
            else:
                print('Starting tushu. Check its status with `status`.')
                with ctx:
                    do_main()
        else:
            print('Invalid command. Commands: `start`, `stop`, `status`')

if __name__ == "__main__":
    args = sys.argv
    try:
        arg = args[1]
        run_daemon(arg)
    except IndexError as e:
        print('Command not found. Commands: `start`,`stop`,`status`')
