import random
import copy

from rply import LexerGenerator
from rply import ParserGenerator
from rply.token import BaseBox


lg = LexerGenerator()

lg.add('NUMBER', r'\d+') 
lg.add('PLUS', r'\+') 
lg.add('MINUS', r'-') 
lg.add('MUL', r'\*') 
lg.add('DIV', r'/') 
lg.add('POW', r'\^') 
lg.add('OPEN_PARENS', r'\(') 
lg.add('CLOSE_PARENS', r'\)') 
lg.add('ROLL', r'd') 
lg.add('KEEP', r'k') 
lg.add('LOW', r'l') 
lg.add('HIGH', r'h') 
lg.add('EXPLODE',r'\!') 
lg.add('REROLL',r'r') 
lg.add('SUCCESS',r's')
lg.add('FAILURE',r'f')
lg.add('BOOLEAN', r'\?') 
lg.add('OPTION', r'\:') 
lg.add('GREATER',r'\>') 
lg.add('LESSER',r'\<') 
lg.add('AND',r'\&') 
lg.add('OR',r'\|') 
lg.add('NEGATE',r'\~') 
lg.add('EQUAL',r'\=')  

pg = ParserGenerator(
    # A list of all token names, accepted by the parser.
    ['NUMBER', 'ROLL', 'LOW', 'HIGH', 'KEEP', 'EXPLODE', 'FAILURE', 'SUCCESS', 'REROLL',
     'OPEN_PARENS', 'CLOSE_PARENS', 'NEGATE',
     'GREATER', 'LESSER', 'EQUAL', 'BOOLEAN', 'OPTION', 'AND', 'OR',
     'PLUS', 'MINUS', 'MUL', 'DIV','POW'
    ],
    # A list of precedence rules with ascending precedence, to
    # disambiguate ambiguous production rules.
    precedence=[
        ('left', ['BOOLEAN', 'OPTION']),
        ('left', ['NEGATE']),
        ('left', ['AND','OR']),
        ('left', ['GREATER','LESSER','EQUAL']),
        ('left', ['PLUS', 'MINUS']),
        ('left', ['MUL', 'DIV']),
        ('left', ['POW']),
    	('left', ['NUMBER']),
        ('left', ['SUCCESS','FAILURE']),
        ('left', ['KEEP','LOW','HIGH','REROLL','EXPLODE']),
        ('left', ['ROLL']),
        ('left', ['OPEN_PARENS', 'CLOSE_PARENS']),
    ]
)

cross_out = lambda x : '~~{}~~'.format(x)
bold = lambda x : '**{}**'.format(x)

class DiceError(Exception):
    pass

class Number(BaseBox):
    def __init__(self, value):
        self.value = value

    def __repr__(self):
        return "{}".format(self.value)

    def eval(self):
        return self.value

arithmetic={
        'PLUS':lambda x,y: x+y,
        'MINUS':lambda x,y: x-y,
        'MUL':lambda x,y: x*y,
        'DIV':lambda x,y: x//y,
        'POW':lambda x,y: x**y
        }
arithmetic_symbols={
        'PLUS':'+',
        'MINUS':'-',
        'MUL':'*',
        'DIV':'/',
        'POW':'^'
        }
class MathOp(BaseBox):
    def __init__(self, left, command, right):
        self.left = left
        self.symbol=arithmetic_symbols[command]
        self.right = right
        self.solution = arithmetic[command](self.left.eval(),self.right.eval())

    def __repr__(self):
        return "{}{}{}".format(self.left,self.symbol,self.right)

    def eval(self):
        return self.solution

class DiceOp(BaseBox):
    def __init__(self, left, right):
        self.left = copy.deepcopy(left)
        self.right = copy.deepcopy(right)
        self.number_of_dice = left.eval()
        self.size_of_dice = right.eval()
        self.results = [random.randint(1,self.size_of_dice) for i in range(self.number_of_dice)]
        initial_roll = "{}d{}".format(self.left, self.right)
        initial_mod = "[{}]".format(",".join(map(str,self.results)))
        self.modifications=[[initial_roll, initial_mod]]

    def __repr__(self):
        repr_roll=""
        repr_results=""
        for modification in self.modifications:
            repr_roll = repr_roll+modification[0]
            repr_results = repr_results+"->{}".format(modification[1])
        repr_results = repr_results+"->{}".format(self.eval())
        return "({}{})".format(repr_roll,repr_results)

    def keep(self, limit):
        sorted_roll = sorted(self.results,reverse=True)
        keep = sorted_roll[0:limit]
        toss = sorted_roll[limit:len(sorted_roll)]
        self.results = keep
        repr_result = list(map(str,keep))+list(map(cross_out,toss))
        repr_result = "[{}]".format(",".join(repr_result))
        self.modifications.append(['k{}'.format(limit),repr_result])

    def low(self, limit):
        sorted_roll = sorted(self.results)
        keep = sorted_roll[0:limit]
        toss = sorted_roll[limit:len(sorted_roll)]
        self.results = keep
        repr_result = list(map(str,keep))+list(map(cross_out,toss))
        repr_result = "[{}]".format(",".join(repr_result))
        self.modifications.append(['l{}'.format(limit),repr_result])

    def reroll(self, limit):
        repr_result = self.results[:]
        for i in range(0,len(self.results)):
            if self.results[i] <= limit:
                self.results[i] = random.randint(1,self.size_of_dice)
                repr_result[i] = bold(self.results[i])
        repr_result = "[{}]".format(",".join(map(str,repr_result)))
        self.modifications.append(['r{}'.format(limit),repr_result])

    def explode(self,limit):
        #prevent dangerous looping
        if limit < 2:
            limit = 2
        repr_result = self.results[:]
        for i in range(0,len(self.results)):
            if self.results[i] >= limit:
                 new_roll = random.randint(1,self.size_of_dice)
                 self.results.append(new_roll)
                 while new_roll >= limit:
                     new_roll = random.randint(1,self.size_of_dice)
                     self.results.append(new_roll)
                     repr_result.append(bold(new_roll))
        repr_result = "[{}]".format(",".join(map(str,repr_result)))
        self.modifications.append(['!{}'.format(limit),repr_result])

    def success(self, limit):
        keep = [x for x in self.results if x>limit]
        toss = [x for x in self.results if x<limit]
        self.results = [1] * len(keep)
        repr_result = list(map(str,keep))+list(map(cross_out,toss))
        repr_result = "[{}]".format(",".join(repr_result))
        self.modifications.append(['s{}'.format(limit),repr_result])

    def failure(self, limit):
        keep = [x for x in self.results if x<limit]
        toss = [x for x in self.results if x>limit]
        self.results = [1] * len(keep)
        repr_result = list(map(str,keep))+list(map(cross_out,toss))
        repr_result = "[{}]".format(",".join(repr_result))
        self.modifications.append(['f{}'.format(limit),repr_result])

    def modify(self,command,limit):
        modifications={
                'KEEP':self.keep,
                'HIGH':self.keep,
                'LOW':self.low,
                'REROLL':self.reroll,
                'EXPLODE':self.explode,
                'SUCCESS':self.success,
                'FAILURE':self.failure,
                }
        modifications[command](limit)

    def eval(self):
        return sum(self.results)

boolean={
        'GREATER':lambda x,y: x>y,
        'LESSER':lambda x,y: x<y,
        'EQUAL':lambda x,y: x==y,
        'AND':lambda x,y: x and y,
        'OR':lambda x,y: x or y
        }
boolean_symbols={
        'GREATER':'>',
        'LESSER':'<',
        'EQUAL':'==',
        'AND':'&',
        'OR':'|'
        }
class BooleanOp(BaseBox):
    def __init__(self, left, command, right):
        self.left = left
        self.symbol = boolean_symbols[command]
        self.right = right
        self.solution = bool(boolean[command](self.left.eval(),self.right.eval()))
        self.inverted = False

    def __repr__(self):
        self.negate_symbol=""
        if self.inverted:
            self.negate_symbol="~"
        return "({}{}{}{}={})".format(self.negate_symbol,self.left,self.symbol,self.right,self.solution)
    def invert(self):
        self.solution = not self.solution
        self.inverted = not self.inverted

    def eval(self):
        return self.solution

class ParensOp(BaseBox):
    def __init__(self, operation):
        self.operation = operation

    def __repr__(self):
        return "({})".format(self.operation)

    def eval(self):
        return self.operation.eval()

class TernaryOp(BaseBox):
    def __init__(self,true,false,boolean):
        self.true = true
        self.false = false
        self.boolean = boolean
        if self.boolean.eval():
            self.result = self.true
        else:
            self.result = self.false

    def __repr__(self):
        if self.boolean.eval():
            return "{}?{}:~~{}~~".format(self.boolean, self.true, self.false)
        else:
            return "{}?~~{}~~:{}".format(self.boolean, self.true, self.false)
    def eval(self):
        return self.result.eval()

@pg.production('expression : NUMBER')
def expression_number(p):
    return Number(int(p[0].getstr()))

@pg.production('expression : expression BOOLEAN expression OPTION expression')
def expression_ternary(p):
    return TernaryOp(p[2],p[4],p[0])

@pg.production('expression : OPEN_PARENS expression CLOSE_PARENS')
def expression_parens(p):
    return ParensOp(p[1])

@pg.production('expression : NEGATE expression')
def expression_negate(p):
    boolean = p[1]
    if type(boolean) == BooleanOp:
        boolean.invert()
    elif type(boolean) == ParensOp:
        if type(boolean.operation) == BooleanOp:
            boolean.operation.invert()
    else:
        raise AssertionError('Only applicable to booleans')
    return boolean

@pg.production('expression : expression GREATER expression')
@pg.production('expression : expression LESSER expression')
@pg.production('expression : expression EQUAL expression')
@pg.production('expression : expression AND expression')
@pg.production('expression : expression OR expression')
def expression_boolop(p):
    left = p[0]
    command = p[1].gettokentype()
    right = p[2]
    return BooleanOp(left,command,right)

@pg.production('expression : expression ROLL expression')
def expression_diceop(p):
    left = p[0]
    right = p[2]
    return DiceOp(left,right)

@pg.production('expression : expression KEEP expression')
@pg.production('expression : expression LOW expression')
@pg.production('expression : expression HIGH expression')
@pg.production('expression : expression REROLL expression')
@pg.production('expression : expression EXPLODE expression')
@pg.production('expression : expression SUCCESS expression')
@pg.production('expression : expression FAILURE expression')
def expression_modify_diceop(p):
    left = p[0]
    command = p[1].gettokentype()
    right = int(p[2].eval())
    left.modify(command, right)
    return left

@pg.production('expression : expression PLUS expression')
@pg.production('expression : expression MINUS expression')
@pg.production('expression : expression MUL expression')
@pg.production('expression : expression DIV expression')
@pg.production('expression : expression POW expression')
def expression_mathop(p):
    left = p[0]
    command = p[1].gettokentype()
    right = p[2]
    return MathOp(left,command,right)

@pg.error
def error_handler(token):
    raise DiceError("Incorrect dice algebra")

lexer = lg.build()
parser = pg.build()
