from .util import set_stats
from .util import StatError
async def edit(client, message, data, character, characters, configs):
    """
    /edit __name__ [field=value]...
    Edits the values of fields
    """
    if character is not None:
        try:
            await set_stats(data, character, characters, configs)
            await client.send_message(message.author, "{} was succesfully updated. View their sheet with `/sheet`.".format(character['name']))
        except StatError as e:
            await client.send_message(message.author, "Configuration Problem. Error: {}".format(e))
    else:
        await client.send_message(message.author, "Character must be selected")
