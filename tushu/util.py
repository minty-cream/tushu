import string
import re
from rply.errors import LexingError
from .gushi import dice_algebra
DiceError = dice_algebra.DiceError
parser = dice_algebra.parser
lexer = dice_algebra.lexer

class StatError(Exception):
    pass

async def set_stats(data, character, characters, configs):
    character_id = {'_id':character['_id']}
    updated = False
    fields = {'approved':False,'waiting_for_approval':False}
    errors = []
    deletions = {}
    blacklist = ['active','waiting_for_approval','approved']
    character_transaction = await characters.find_one(character_id)
    for field, setting in data.items():
        field = field.lower()
        if field in blacklist:
            errors.append('Cannot edit `{}` manually'.format(",".join(blacklist)))
            continue
        if not setting:
            deletions[field]=""
            if field in character_transaction:
                del character_transaction[field]
            continue
        field_type, formatted_setting = format_field(setting)
        field_config = await configs.find_one({'field':field})
        if field_config is not None:
            if field_config['type'] == 'calculation':
                errors.append('Default fault. Cannot manually set default calculation.')
                continue
            if field_config['type'] != field_type:
                errors.append('Default fault. Must be {}.'.format(field_type))
                continue
        fields[field]=formatted_setting
        character_transaction[field]=formatted_setting

    calculations = configs.find({'type':'calculation'})
    async for calculation in calculations:
        field = calculation['field']
        field_default = calculation['default']
        try:
            calculation_roll = format_roll(field_default,character_transaction)
            fields[field] = str(parser.parse(lexer.lex(calculation_roll)).eval())
        except (LexingError, DiceError) as e:
            errors.append('Calculation Fault. Error: {}'.format(e))

    if len(errors) > 0:
        raise StatError(",".join(errors))

    if len(fields) > 0:
        await characters.update_one(character_id,{'$set':fields})

    if len(deletions) > 0:
        await characters.update_one(character_id,{'$unset':deletions})



class RollFormatter(string.Formatter):
    def get_value(self, key, args, kwargs):
        result = kwargs.get(key,'0')
        return result

def format_roll(roll, character):
    if character is None:
        return zero_format_roll(roll)

    roll = str(roll).lower()
    roll_formatter = RollFormatter()
    while re.search('{(.*)}',roll) is not None:
        roll = roll_formatter.format(roll, **character)
    return roll

def zero_format_roll(roll):
    roll = str(roll).lower()
    roll =  re.sub('{(.*)}','0',roll)
    return roll

def format_stats(data):
    datasets = data.split(':=')
    settables = {}
    left = datasets[0].strip()
    for x in range(1,len(datasets)-1):
        settable = datasets[x].strip().rpartition(' ')
        right = settable[0]
        settables[left]=right.strip()
        left = settable[2].strip()
    settables[left]=datasets[-1].strip()
    return settables

def format_field(field):
    try:
        zero_formatted_roll = zero_format_roll(field)
        roll = str(parser.parse(lexer.lex(zero_formatted_roll)).eval())
        if zero_formatted_roll != field:
            return ('calculation', field.lower())
        else:
            return ('number', roll)
    except (LexingError, DiceError) as e:
        return ('text', field)
