from .util import set_stats
from .util import StatError

async def generate(client,message, data, character,characters,configs):
    """
    Usage: /generate __name__ [stat=value]...
    Creates a new character with the stats
    """
    if character is not None and not character['active']:
        await client.send_message(message.author, 'That character already exists. Edit them with `/edit`')
        return
    if data is None:
        await client.send_message(message.channel, "Usage: `/generate [name] <stat=value>...`")
        return

    try:
        new_character = {
                'name': data['name'],
                'author_id': message.author.id,
                'approved': False,
                'active': False,
                'waiting_for_approval': False
        }
        del data['name']
        created_character = await characters.insert_one(new_character)
        character_id = {'_id':created_character.inserted_id}
        await set_stats(data,character_id,characters,configs)
        await client.send_message(message.channel, "{} created. Select them with `/alt {}`".format(new_character['name'],new_character['name']))
    except KeyError as e:
        await client.send_message(message.author, "Character Creation Problem. Missing a required variable: {}`".format(e))
    except StatError as e:
        await client.send_message(message.author, "Character Creation Problem. Error: {}".format(e))
