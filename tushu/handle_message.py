from .whitelist import funcs
from .util import format_stats

def get_giblets(command='',name='',data=''):
    return [command.strip(), name.strip(), data.strip()]

async def handle_message(client,message,db):
    """Receive a message and break it into parts, then throws it to the business logic.
    """
    if message.content.startswith('/'):
        characters = db['characters'][message.server.id]
        configs = db['configs'][message.server.id]

        character = None

        content_string = message.content.lstrip('/')
        contents = content_string.split('"',maxsplit=2)
        has_no_quote = len(contents) != 3
        if has_no_quote:
            contents = content_string.split(' ',maxsplit=2)

        command, name, data = get_giblets(*contents)

        if not data:
            data = name

        if ':=' in name:
            data = name+' '+data
            name = None

        character = await characters.find_one({
            'name': name,
            'author_id': message.author.id
        })
        if character is None:
            character = await characters.find_one({
                'author_id': message.author.id,
                'active': True,
            })

        data = data.strip()
        if ':=' in data:
            data = format_stats(data)
            if name:
                data.update({'name':name})
        else:
            data = data.split(' ')

        await funcs[command](client,message, data, character, characters, configs)
