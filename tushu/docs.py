async def show_invite(client,message, data, character,characters,configs):
    await client.send_message(message.channel, 'You can add me to your server using this link!\n https://discordapp.com/api/oauth2/authorize?client_id={}&permissions=18432&scope=bot'.format(os.getenv('CLIENT_ID')))

async def show_help(client,message, data, character,characters,configs):
    files = {
            'basic':
            (
                'Tu Shu is a roll bot that stores your character sheets and has a powerful macro language.\n'
                'Here is all of my commands.\n\n'
                '\n*optional parameter*\n'
                '__required parameter__\n'
                '\n'
                '/generate __name__ *stat:=value* `Creates a new character with the stats. You can have any amount of field:=value combinations in one line`\n'
                '/roll *name* __dice_notation_string__ `Rolls the dice.`\n'
                '/configure __field:=type__*:=default* `Configure the character sheet (GM Only) You can have any amount of field:=value combinations in one line`\n'
                '\n'
                '/help `show this list`\n'
                '/help dice_algebra `get advanced help on the roll command`\n'
                '/help configure `get help on the configure command`\n'
                '\n'
                '/sheet `Shows the sheet of your current alt`\n'
                '/sheet __name__ `Shows the sheet of the selected character owned by you`\n'
                '/sheet __name__ __@mention__ `Shows the sheet of the selected character owned by mentioned player`\n'
                '\n'
                '/approve __name__ `Request a GM stamp the character ready to play`\n'
                '/edit __name__ __field:=value__ `Edits the values of fields. You can have any amount of field:=value combinations in one line`\n'
                '/ooc `Changes you to Out of Character`\n'
                '/alt __name__ `Changes your active character`\n'
                '/alt __@mention__ `See who mentioned user is playing as`\n'
                '/alt `Get a list of your alts`\n'
                '\n'
                '/invite `Get a link to invite this bot to your own server`'
                ),
            'dice_algebra':
            (
                'These are all valid strings for any field that accepts dice_algebra such as rolls, configures, and edits\n'
                'Dice algebra is a powerful macro language that allows you to get random numbers, do math, deal with variables, and make decisions based on boolean algebra\n'
                '\n'
                '***Standard dice usage***\n'
                '`1d20` Get a random number between 1 and 20\n'
                '`1d20+5` Get the sum of 5 and a random number between 1 and 20.\n'
                '\tSupported math symbols: \n'
                '\t`*` (multiply), `/` (divide), `+` (addition), `-` (subtraction), `^` (exponentiation)\n'
                '\tNote: Division automatically rounds down. Add +1 if you want to round up.\n'
                '`3d6` Get the sum of 3 random numbers between 1 and 6\n'
                '`{str}d6` {str} will be replaced with the character\'s "str" stat. "str" can be more dice algebra.\n'
                '`3d6k2` or `3d6h2` Keep Highest. Get the sum of the 2 highest results of 3 random numbers between 1 and 6\n'
                '`3d6l2` Keep Lowest. Get the sum of the 2 lowest results of 3 random numbers between 1 and 6\n'
                '`3d6s2` Remove under. Get the sum of dice above 2 in a group of 3 random numbers between 1 and 6\n'
                '`3d6f2` Remove over. Get the sum of dice under 2 in a group of 3 random numbers between 1 and 6\n'
                '`3d6r2` Reroll. Roll 3 die. Reroll the 2 lowest die. Get the sum.\n'
                '`3d6!2` Explode. Roll 3 die. Roll an additional die if any are greater than 2. Get the sum of all rolls.\n'
                '\n'
                '***Complex dice algebra***\n'
                '\t`()` You can use parenthesis to indicate order of operations priority as in standard mathematics.'
                '\t Order of operations is Parenthesis, multiple dice rolls, dice rolls, boolean resolution, ternary operator resolution, exponentiation, multiplication and division, addition and subtraction.'
                '\n'
                '***Boolean dice algebra***\n'
                '`{str}>4?{str}:{dex}` Ternary operation. It is suggested you contain these in parenthesis. Template is `[boolean]?[true resolution]:[false resolution]`\n The example means "If the str value is over four, return the str value, otherwise return the dex value."'
                '\tSupported comparison symbols:\n'
                '\t`>` (greater), `<` (lesser), `=` (equal)\n'
                '\tSupported boolean symbols:\n'
                '\t`&` (AND) , `|` (OR) , `~` (NEGATE (requires parenthesis)), `=` (EQUAL)'
                ),
            'configure':
                (
                        "***GM Only***\n"
                        "Configure the list of fields on the character sheet\n"
                        "\n"
                        "If you enter just a field's name, it will delete the field.\n"
                        "\n"
                        "number\n"
                        ":   A player editable plain number, good for HP or AC.\n"
                        "\n"
                        "text\n"
                        ":   Player editable plain text. Cannot be interacted with, but can be put into the character sheet template.\n"
                        "\n"
                        "[algebra notation]\n"
                        ":   Instead of entering number, or text, you can just write down some algebra notation in the 'type' field. This is good for automated stats. Cannot have a default. This is calculated any time a user edits their stats, so be careful when using dice. See `/help dice_algebra` for more info.\n"
                        )
                }
    try:
        if data[0]:
            _file = data[0]
            _help = files[_file]
        else:
            _help = files['basic']
    except KeyError:
        _help = (
                'No help file by that name'
                )
    await client.send_message(message.author, _help)
