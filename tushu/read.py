import discord

async def who(client, message, data, character, characters, configs):
    """
    Usage: /who
    Get a list of everyone's alts
    """
    _characters = characters.find({'active':True})
    _users = []
    async for _character in _characters:
        _name = _character['name']
        _author_id = _character['author_id']
        _value = "<@{}>".format(_author_id)
        _user = {'name':_name,'value':_value}
        _users.append(_user)
    _list=discord.Embed(title="Active Characters in {}".format(message.server.name))
    for _user in _users:
        _list.add_field(**_user)
    await client.send_message(message.author, embed=_list)

async def sheet(client, message, data, character, characters, configs):
    """
    Usage: /sheet
    Shows the sheet of your current alt
    Usage: /sheet __name__
    Shows the sheet of the selected character owned by you
    Usage: /sheet __name__ @mention
    Shows the sheet of the selected character owned by mentioned player
    """
    #display only stats from the character that is in config's numbers, texts, and calculation fields
    _configs = configs.find({ "type":{"$in":["number","text","calculation"]} })
    _author = message.author

    if data is not None and len(data) > 0:
        _name = data[0]
        if _name:
            if len(message.mentions) > 0:
                _author = message.mentions[0]
            _character = await characters.find_one({'name': _name, 'author_id': _author.id})
            if _character is not None:
                character = _character

    if character is not None:
        _certs="**Player** <@{author_id}>\n".format(**character)
        if(character['approved']):
            _certs = _certs + ":white_check_mark: **Approved**\n"
        else:
            _certs = _certs + ":x: **Not Approved**\n"
        sheet=discord.Embed(title="**Character** {name}".format(**character),description=_certs)
        async for config in _configs:
            _field = config['field']
            if _field in character:
                _stat=character[_field]
            else:
                _stat="UNPOPULATED"
            sheet.add_field(name=_field.capitalize(),value=_stat)
        await client.send_message(message.channel, embed=sheet)
    else:
        await client.send_message(message.channel,'Could not find character.')
