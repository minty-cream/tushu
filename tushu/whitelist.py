from . import business
from . import config
from . import create
from . import docs
from . import read
from . import update
from . import util

funcs = {
        'alt': business.alt,
        'approve': business.approve,
        'ooc': business.ooc,
        'roll': business.roll,
        'r': business.roll,
        'configure': config.configure,
        'config': config.configure,
        'generate': create.generate,
        'gen': create.generate,
        'help': docs.show_help,
        'invite': docs.show_invite,
        'sheet': read.sheet,
        'who': read.who,
        'edit': update.edit,
        'set': update.edit
        }
