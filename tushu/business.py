from .gushi import dice_algebra
from rply.errors import LexingError
parser = dice_algebra.parser
lexer = dice_algebra.lexer
DiceError = dice_algebra.DiceError
from .util import format_roll

async def alt(client,message, data, character,characters,configs):
    """
    Usage: /alt __name__
    Changes your active character
    Usage: /alt @mention
    See who mentioned user is playing as
    Usage: /alt
    Get a list of your alts
    """
    if len(message.mentions) > 0:
        _author = message.mentions[0]
        __character = await characters.find_one({'active':True,'author_id':_author.id})

        if __character is None:
            await client.send_message(message.channel, "{} is not playing as anyone.".format(_author.name))
        else:
            await client.send_message(message.channel, "{} is playing as {}.".format(_author.name,__character['name']))
    elif data is None:
        _characters = characters.find({'author_id':message.author.id})
        character_list = "***Your Alts***\n"
        active_character = await characters.find_one({'author_id':message.author.id,'active':True})
        if active_character is not None:
            character_list = character_list + "**Current Character** {}\n".format(active_character['name'])
        else:
            character_list = character_list + "**No Character Currently Selected**\n"
        async for __character in _characters:
            character_list = character_list+"{}\n".format(__character['name'])
        if character_list:
            await client.send_message(message.author, character_list)
        else:
            await client.send_message(message.author, "You have no characters on this server. Use `/generate` to make one! Check `/help` for information on how to use it.")
    elif character:
        await characters.update_many({'author_id': message.author.id},{"$set":{'active':False}})
        await characters.update_one(character,{"$set":{'active':True}})
        await client.send_message(message.channel, "{} is now playing as {}".format(message.author.name,character['name']))
    else:
        await client.send_message(message.channel, "Character was not found.")

async def ooc(client,message, data, character,characters,configs):
    """
    Usage: /ooc
    Changes you to Out of Character
    """
    await characters.update_many({'author_id': message.author.id},{"$set":{'active':False}})
    await client.send_message(message.channel, "{} is now OOC".format(message.author.name))

async def approve(client,message, data, character,characters,configs):
    """
    Usage: /approve __name__
    Request a GM stamp the character ready to play
    """
    if character is not None:
        if not character['waiting_for_approval']:
            #compare all stats in character to config's numbers, texts, and calculation fields
            await characters.update_one(character,{'$set':{'waiting_for_approval':True}})
            _configs = configs.find({ "type":{"$in":["number","text","calculation"]} })
            sheet="Do you approve?\n"
            sheet=sheet+"**Name** {}\n".format(character['name'])
            sheet=sheet+"**Player** {}\n".format(message.author.name)
            async for config in _configs:
                _field = config['field']
                if _field in character:
                    _stat=character[_field]
                else:
                    _stat="UNPOPULATED"
                sheet = sheet+"**{}** {}\n".format(_field.capitalize(),_stat)
            sheet=sheet+":thumbsup: to approve or :thumbsdown: to disapprove"
            request = await client.send_message(message.server.owner, sheet)
            response = await client.wait_for_reaction(message=request,user=message.server.owner)
            _emoji = response.reaction.emoji
            if(_emoji.startswith(emoji.emojize(':thumbs_up:'))):
                await characters.update_one(character,{'$set':{'approved':True,'waiting_for_approval':False}})
                await client.send_message(message.author, "{} was **approved**. :white_check_mark:".format(character['name']))
                await client.send_message(message.channel, "{} was **approved**. :white_check_mark:".format(character['name']))
            if(_emoji.startswith(emoji.emojize(':thumbs_down:'))):
                await characters.update_one(character,{'$set':{'approved':False,'waiting_for_approval':False}})
                await client.send_message(message.author, "{} was **not** approved. :x:".format(character['name']))
        else:
            if character['waiting_for_approval']:
                await client.send_message(message.author, "{} is already awaiting approval. :x:".format(character['name']))
    else:
        await client.send_message(message.author, "Please designate a character or select an alt with the `/alt` command")

async def roll(client,message, data, character,characters,configs):
    """
    /roll [name] [dice_notation_string]
    Rolls the dice
    """
    try:
        roll_string = "".join(data)
        roll_string = format_roll(roll_string, character)
        roll = parser.parse(lexer.lex(roll_string))
        await client.send_message(message.channel, "{} {}\nResult: {}".format(message.author.mention, roll, roll.eval()))
    except (LexingError, DiceError) as e:
        await client.send_message(message.channel, "Incorrect dice algebra.")
