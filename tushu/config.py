from pymongo import ReturnDocument
from rply.errors import LexingError

from .gushi import dice_algebra
DiceError = dice_algebra.DiceError
parser = dice_algebra.parser
lexer = dice_algebra.lexer
from .util import format_roll
from .util import zero_format_roll
from .util import format_field
from .util import set_stats
from .util import StatError
from .read import sheet

async def configure(client,message, data, character,characters,configs):
    """
    !configure __field=type__[=default]...

    GM Only
    Configure the list of fields on the character sheet

    If you enter just a field's name, it will delete the field.

    number
    :   A player editable plain number, good for HP or AC.

    text
    :   Player editable plain text. Cannot be interacted with, but can be put into the character sheet template.

    [algebra notation]
    :   Instead of entering number, or text, you can just write down some algebra notation in the "type" field. This is good for automated stats. Cannot have a default.
    """

    test_character = {
            'name': 'TEST_CHARACTER',
            'author_id': message.server.id
    }

    if type(data) is not dict:
        _character = await characters.find_one(test_character)
        if _character is None:
            await client.send_message(message.channel,"Test character has not been created.")
        else:
            await sheet(client, message, data, _character, characters, configs)
        return

    if(message.author.id != message.server.owner.id):
        await client.send_message(message.author,"Configuration can only be done by the server owner.")
        return

    _default_fields = { }
    _character = await characters.find_one_and_update(
        test_character,
        {'$setOnInsert': test_character},
        upsert=True,
        return_document=ReturnDocument.AFTER
        )
    _character_id = {'_id':_character['_id']}

    for field,default in data.items():
        field = field.lower()
        if default is None:
            _default_fields[field]=None
            await configs.delete_one({'field':field})
            continue

        field_type, formatted_default = format_field(default)
        default = formatted_default
        if field_type != 'calculation':
            _default_fields[field] = formatted_default

        await configs.update_one({'field':field},{'$set':{'field':field,'type':field_type,'default':default}},True)

    try:
        await set_stats(_default_fields, _character, characters, configs)
        await characters.update_many({"author_id":{"$ne":message.server.id}},{'$set':{'approved':False, 'active':False}})
        await client.send_message(message.channel, 'Configuration complete. View test character with `/config`. All characters have been unapproved.')
    except StatError as e:
        await client.send_message(message.channel, 'Configuration produced an invalid test character. Error: {}'.format(e))
