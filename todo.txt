#todo
#
#test it.
#
#works
#configure should probably update an example character automatically to verify calculations against using populate_roll
#commands should automatically use your currently selected alt if you don't include specifiers
#stats and macros
#needs to handle multiple servers, just change all the collection calls to collection[message.server.id]
#make dice algebra reflect documentation
#clean up
#   change hard values like the mongostring to configurables
#   update usage instructions
#   add usage instructions
#   add command aliases

#future todo
#clean up
#   seperate functions into individual well documented files
#phase 1
#   validation code should be lightly configurable to allow for limitations
#   bot should be able to get approval from a role instead of the server owner
#   gms need to be able to configure more stuff. 
#      Where to direct approvals towards
#phase 2
#   bot needs to be able to start stories in designated section
#      started stories need to include a pinned post with a small prompt about the story, and an emoji legend for the included characters
#           should the GM get bot assistance, or should the GM just be expected to be disciplined enough to notify which characters are speaking?
#      post annotations to indicate what character said what
#phase 3
#   wikis
#       bot should be able to track information set by the gm
#       bot should scan posts for existing articles, and provide links to articles on request
#       bot should be pm'able for wiki articles
#   notes
#       bot should be able to track notes given to it by players
#       bot should be pm'able for notes

#wishlist
#voice to text
#   automated voice chat transcription
#machine learning
#   automated voice chat gameplay minutes taking
#   automated text game minutes taking
#   automatic setting notes and etc reminders via voice chat
#   automated voice activated rolling
#voice game-text game integration
#   dictate voice chat to text
#   text to speech with in-character voice
