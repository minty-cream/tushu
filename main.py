def do_main():
    import asyncio
    import discord
    import motor.motor_asyncio
    import os
    from dotenv import load_dotenv
    from tushu.handle_message import handle_message
    load_dotenv()

    client = discord.Client()
    mongo_client = motor.motor_asyncio.AsyncIOMotorClient(os.getenv('MONGODB'))
    db = mongo_client[os.getenv('TUSHUDB')]

    @client.event
    async def on_ready():
        print('Logged in as')
        print(client.user.name)
        print(client.user.id)
        print('------')

    @client.event
    async def on_message(message):
        """Handle incoming messages by sending them to a message handler"""
        await handle_message(client, message, db)

    client.run(os.getenv('API_KEY'))

if __name__ == '__main__':
    do_main()
